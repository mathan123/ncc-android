package com.agero.ncc.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class JobDetailCallBottomSheetDialog extends BottomSheetDialogFragment {

    HomeActivity mHomeActivity;
    CallDialogClickListener callDialogClickListener;

    @BindView(R.id.image_call_dispatcher)
    ImageView mImageCallDispatcher;
    @BindView(R.id.text_label_dispatcher)
    TextView mTextCallDispatcher;

    public static JobDetailCallBottomSheetDialog getInstance() {
        return new JobDetailCallBottomSheetDialog();
    }

    public void setJobDetailCallDialogClickListener(CallDialogClickListener callDialogClickListener) {
        this.callDialogClickListener = callDialogClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_job_detail_call_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        if(!mHomeActivity.isLoggedInDriver()){
            mImageCallDispatcher.setVisibility(View.GONE);
            mTextCallDispatcher.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = (FrameLayout)
                        dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }

    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
    @OnClick({R.id.text_label_customer, R.id.text_label_dispatcher, R.id.text_label_agero, R.id.image_call_customer, R.id.image_call_dispatcher, R.id.image_call_agero})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_customer:
            case R.id.image_call_customer:
                eventAction = NccConstants.FirebaseEventAction.AS_CALL_CUSTOMER;
                if (callDialogClickListener != null) {
                    callDialogClickListener.onCustomerClick();
                }
                break;
            case R.id.text_label_dispatcher:
            case R.id.image_call_dispatcher:
                eventAction = NccConstants.FirebaseEventAction.AS_CALL_DISPATCHER;
                if (callDialogClickListener != null) {
                    callDialogClickListener.onDispatcherClick();
                }
                break;
            case R.id.text_label_agero:
            case R.id.image_call_agero:
                eventAction = NccConstants.FirebaseEventAction.AS_CALL_AGERO;
                if (callDialogClickListener != null) {
                    callDialogClickListener.onAgeroClick();
                }
                break;
        }
        mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
    }

    public interface CallDialogClickListener {

        public void onCustomerClick();

        public void onDispatcherClick();

        public void onAgeroClick();

    }
}
