package com.agero.ncc.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.TermsAndConditionsAcceptenceModel;
import com.agero.ncc.model.TermsAndConditionsModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TermsAndConditionsFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    @Inject
    NccApi nccApi;

    private UserError mUserError;
    private String mTermsVersion;

    @BindView(R.id.webview_legal)
    WebView legalWebview;
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.LEGAL;

    public TermsAndConditionsFragment() {
        // Intentionally empty
    }

    public static TermsAndConditionsFragment newInstance() {
        TermsAndConditionsFragment fragment = new TermsAndConditionsFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_terms_conditions, fragment_content, true);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideToolBar();
        mHomeActivity.mintlogEvent("Terms and Conditions Launched");

        getContent();
        return superView;
    }

    private void getContent() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        nccApi.getTermsAndConditions(BuildConfig.APIGEE_URL + "/profile/terms-and-conditions/terms-conditions", "Bearer " + mPrefs.getString(NccConstants.APIGEE_TOKEN, "")).enqueue(new Callback<TermsAndConditionsModel>() {
                            @Override
                            public void onResponse(Call<TermsAndConditionsModel> call, Response<TermsAndConditionsModel> response) {
                                if (isAdded()) {
                                    if (response != null && response.body() != null) {
                                        hideProgress();
                                        TermsAndConditionsModel responseModel = response.body();
//                                        mTermsContent.setText(responseModel.getContent());
//                                        mTermsVersion = responseModel.getVersion();

                                        //                            mTermsContent.setText(responseModel.getContent());

                                        if (responseModel != null) {
                                            HashMap<String, String> extraDatas = new HashMap<>();
                                            extraDatas.put("json", new Gson().toJson(responseModel));
                                            mHomeActivity.mintlogEventExtraData("Terms and Conditions Screen Terms and Conditions", extraDatas);
                                        }

                                        if(responseModel != null && responseModel.getContent() != null) {
                                            String htmlAsString = responseModel.getContent();      // used by WebView
                                            Spanned htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView
                                            legalWebview.loadDataWithBaseURL(null, htmlAsString, "text/html", "UTF-8", null);
                                        }
                                    } else if (response.errorBody() != null) {
                                        hideProgress();
                                        try {
                                            JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                            if (errorResponse != null) {
                                                if (mHomeActivity != null && !mHomeActivity.isFinishing() && errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                                    Toast.makeText(mHomeActivity, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<TermsAndConditionsModel> call, Throwable t) {
                                hideProgress();
                                if (isAdded()) {
                                    mHomeActivity.mintlogEvent("Terms and Conditions get content Api Error - " + t.getLocalizedMessage());
                                    if (mHomeActivity != null && !mHomeActivity.isFinishing()) {
                                        Toast.makeText(mHomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    @OnClick({R.id.button_agree,R.id.image_account_back_arrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.button_agree:
                updateAcceptance();
                eventAction = NccConstants.FirebaseEventAction.AGREE;
                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                break;
            case R.id.image_account_back_arrow:
                mHomeActivity.finish();
                break;
        }
    }

    private void updateAcceptance() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        nccApi.postTermsAndConditionsAcepetence(BuildConfig.APIGEE_URL + "/profile/terms-and-conditions/terms-conditions/acceptance", "Bearer " + mPrefs.getString(NccConstants.APIGEE_TOKEN, "")).enqueue(new Callback<TermsAndConditionsAcceptenceModel>() {
                            @Override
                            public void onResponse(Call<TermsAndConditionsAcceptenceModel> call, Response<TermsAndConditionsAcceptenceModel> response) {
                                if (isAdded()) {
                                    if (response != null && response.body() != null) {
                                        hideProgress();
                                        HashMap<String,String> extraDatas = new HashMap<>();
                                        extraDatas.put("json",new Gson().toJson(response.body()));
                                        extraDatas.put("auth_token", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                                        mHomeActivity.mintlogEventExtraData("Terms & Conditions Acceptance",extraDatas);

                                        startNextScreen();
                                    } else if (response.errorBody() != null) {
                                        hideProgress();
                                        try {
                                            if (isAdded()) {
                                                JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                                if (errorResponse != null) {
                                                    if (mHomeActivity != null && !mHomeActivity.isFinishing() && errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                                        Toast.makeText(mHomeActivity, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                                    }
                                                    mHomeActivity.mintlogEvent("Terms and Conditions Update Acceptance Api Error - " + (String) errorResponse.get("message"));
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<TermsAndConditionsAcceptenceModel> call, Throwable t) {
                                hideProgress();
                                if (mHomeActivity != null && !mHomeActivity.isFinishing()) {
                                    Toast.makeText(mHomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void startNextScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getContext().checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mHomeActivity.startActiveJobs();
            } else {
                mHomeActivity.push(PermissionFragment.newInstance());
            }

        } else {
            startActivity(new Intent(getActivity(), HomeActivity.class));
            SparkLocationUpdateService.clearList();
            mHomeActivity.finish();
        }
    }
}
