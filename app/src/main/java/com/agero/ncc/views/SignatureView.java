package com.agero.ncc.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;

import java.util.Date;

/**
 * Custom View to draw signature.
 */
public class SignatureView extends View {
    private Paint mPaint;

    private Bitmap mBitmap;

    private Canvas mCanvas;

    private Path mPath = null;

    private Paint mBitmapPaint = null;
    private boolean isSigned;

    SignTouchLisener SignTouchLisener;
    public SignatureView(Context context) {
        super(context);
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(8);

        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mBitmapPaint.setAntiAlias(true);
        mBitmapPaint.setDither(true);
        mBitmapPaint.setColor(Color.BLACK);
        mBitmapPaint.setStyle(Paint.Style.STROKE);
        mBitmapPaint.setStrokeJoin(Paint.Join.ROUND);
        mBitmapPaint.setStrokeCap(Paint.Cap.ROUND);
        mBitmapPaint.setStrokeWidth(8);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    public void undo() {
        isSigned = false;
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    public boolean isSigned() {
        return isSigned;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

        canvas.drawPath(mPath, mPaint);
    }

    private float mX, mY;

    private static final float TOUCH_TOLERANCE = 4;

    private long touchDownTime = 0;

    private long touchUpTime = 0;

    private boolean moveFlag = false;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
        moveFlag = true;
    }

    private void touch_up(float x, float y) {

        if (mX == x && mY == y) {
            long interval = (touchUpTime - touchDownTime);
            if (interval > 50 && !moveFlag)
                mPath.addCircle(x, y, 1.0f, Path.Direction.CW);
        } else
            mPath.lineTo(mX, mY);

        moveFlag = false;
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
        mPath.reset();
    }

    @SuppressLint("ClickableViewAccessibility")
    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        SignTouchLisener.onSignTouchLisener();
        float x = event.getX();
        float y = event.getY();
        isSigned = true;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDownTime = new Date().getTime();
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touchUpTime = new Date().getTime();
                touch_up(x, y);
                invalidate();
                break;
        }
        return true;
    }
    public interface SignTouchLisener {
        void onSignTouchLisener();
    }

    public void setOnSignTouchLisener(SignTouchLisener SignTouchLisener){
        this.SignTouchLisener=SignTouchLisener;
    }
}
