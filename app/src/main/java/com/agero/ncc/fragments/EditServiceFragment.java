package com.agero.ncc.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Guideline;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;

public class EditServiceFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;
    @BindView(R.id.text_edit_service_description)
    TextView mTextEditServiceDescription;
    @BindView(R.id.text_edit_service)
    TextView mTextEditService;
    @BindView(R.id.spinner_edit_service)
    Spinner mSpinnerEditService;
    @BindView(R.id.view_border_edit_service)
    View mViewBorderEditService;
    @BindView(R.id.text_edit_service_reason)
    TextView mTextEditServiceReason;
    @BindView(R.id.spinner_edit_service_reason)
    Spinner mSpinnerEditServiceReason;
    @BindView(R.id.view_border_edit_service_reason)
    View mViewBorderEditServiceReason;
    @BindView(R.id.edit_tow_dest_name)
    EditText mEditTowDestName;
    @BindView(R.id.text_input_tow_dest_name)
    TextInputLayout mTextInputTowDestName;
    @BindView(R.id.edit_address)
    EditText mEditAddress;
    @BindView(R.id.text_input_address)
    TextInputLayout mTextInputAddress;
    @BindView(R.id.edit_city)
    EditText mEditCity;
    @BindView(R.id.text_input_city)
    TextInputLayout mTextInputCity;
    @BindView(R.id.text_label_add_tow_state)
    TextView mTextLabelAddTowState;
    @BindView(R.id.spinner_tow_state)
    Spinner mSpinnerTowState;
    @BindView(R.id.view_border_edit_service_state)
    View mViewBorderEditServiceState;
    @BindView(R.id.edit_zip_code)
    EditText mEditZipCode;
    @BindView(R.id.text_input_zip_code)
    TextInputLayout mTextInputZipCode;
    @BindView(R.id.guideline)
    Guideline guideline;
    private String mDispatcherId = "", mUserName, mCurrentService = "", mUpdatedService;
    Date date;
    SimpleDateFormat mSimpleFormater;
    UserError mUserError;
    private DatabaseReference mJobReference;
    private String mSelectedService, mServerService = "";
    private List<String> mListService;
    private long oldRetryTime = 0;


    TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            enableSave();

        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    public EditServiceFragment() {
    }

    public static EditServiceFragment newInstance(String userName, String mDispatcherId) {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.USERNAME, userName);
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, mDispatcherId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_edit_service, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showToolbar(getString(R.string.edit_service_label));
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.saveDisable();
        mUserError = new UserError();

        date = new Date();
        mSimpleFormater = new SimpleDateFormat("E, MMM d, hh:mm a", Locale.US);

        mListService = Arrays.asList(getResources().getStringArray(R.array.array_service_unsuccessful_service_needed));
        List<String> listReason = Arrays.asList(getResources().getStringArray(R.array.array_edit_service_reason));
        if (getArguments() != null) {
            mUserName = getArguments().getString(NccConstants.USERNAME, "");
            mDispatcherId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER, "");
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mJobReference = database.getReference("ActiveJobs/"
                + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatcherId);
        //mJobReference.keepSynced(true);

        setVisiblityReason(View.GONE);

        mEditAddress.addTextChangedListener(mTextWatcher);
        mEditCity.addTextChangedListener(mTextWatcher);
        mEditTowDestName.addTextChangedListener(mTextWatcher);
        mEditZipCode.addTextChangedListener(mTextWatcher);

        HintSpinner<String> hintSpinnerService = new HintSpinner<>(mSpinnerEditService, new HintAdapter<String>(getActivity(), "Select", mListService),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String item) {

                        mSelectedService = item;

                        if (mSelectedService.equalsIgnoreCase(mServerService)) {
                            mHomeActivity.saveDisable();
                            if (Utils.isTow(mSelectedService)) {
                                setVisiblityReason(View.VISIBLE);
                            } else {
                                setVisiblityReason(View.GONE);
                            }
                        } else {
                            if (Utils.isTow(mSelectedService)) {
                                displayServiceChangedAlert();
                            } else {
                                setVisiblityReason(View.GONE);
                                mHomeActivity.saveEnable();
                            }
                        }

                    }
                });
        hintSpinnerService.init();

        HintSpinner<String> hintSpinnerReason = new HintSpinner<>(mSpinnerEditServiceReason, new HintAdapter<String>(getActivity(), "Select", listReason),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                    }
                });
        hintSpinnerReason.init();

        getDataFromFirebase();

        return superView;
    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        RxFirebaseDatabase.observeValueEvent(mJobReference.child("service"), String.class).subscribe(taskSnapshot -> {
                            if (taskSnapshot != null) {
                                mSelectedService = taskSnapshot;
                                mServerService = taskSnapshot;
                                int index = getIndexByValue(mListService, mSelectedService);
                                if (index != -1) {
                                    mSpinnerEditService.setSelection(index);
                                }

                            }
                            hideProgress();

                        }, throwable -> {
                            hideProgress();
                            if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                                getDataFromFirebase();
                                oldRetryTime = System.currentTimeMillis();
                            }
                        });

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private int getIndexByValue(List<String> mListService, String mSelectedService) {
        int index = -1;
        for (int i = 0; i <
                mListService.size(); i++) {
            if (mSelectedService.equalsIgnoreCase(mListService.get(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void setValueToServer() {
        if (!mSelectedService.equalsIgnoreCase(mServerService)) {
            mHomeActivity.saveDisable();
            if (Utils.isNetworkAvailable()) {
                showProgress();
                TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                    @Override
                    public void onRefreshSuccess() {
                        if (isAdded()) {
                            RxFirebaseDatabase.setValue(mJobReference.child("service"), mSelectedService).subscribe(() -> {
                                hideProgress();
                            }, throwable -> hideProgress());
                        }
                    }

                    @Override
                    public void onRefreshFailure() {
                        hideProgress();
                        if(isAdded()) {
                            mHomeActivity.tokenRefreshFailed();
                        }
                    }
                });
            } else {
                mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        }
    }


    private void setVisiblityReason(int visiblityReason) {
        mTextEditServiceReason.setVisibility(visiblityReason);
        mSpinnerEditServiceReason.setVisibility(visiblityReason);
        mViewBorderEditServiceReason.setVisibility(visiblityReason);
        mTextInputTowDestName.setVisibility(visiblityReason);
        mTextInputAddress.setVisibility(visiblityReason);
        mTextInputCity.setVisibility(visiblityReason);
        mTextLabelAddTowState.setVisibility(visiblityReason);
        mSpinnerTowState.setVisibility(visiblityReason);
        mViewBorderEditServiceState.setVisibility(visiblityReason);
        mTextInputZipCode.setVisibility(visiblityReason);
    }

    @Override
    public void onSave() {

        setValueToServer();
    }

    private void displayServiceChangedAlert() {
        mUpdatedService = mSpinnerEditService.getSelectedItem().toString();
        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml(getString(R.string.edit_service_alert_label)), Html.fromHtml("<font color='#999999'>" + "Service: " + mUpdatedService + ". Requested by " + mUserName + " " + getString(R.string.dot) + " " + mSimpleFormater.format(date) + "<br><br>" + "Original Service: " + mCurrentService + "</font>")
                , "", Html.fromHtml("<b>" + getString(R.string.ok) + "</b>"));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                setVisiblityReason(View.VISIBLE);
            }
        });
        alert.show(getFragmentManager().beginTransaction(), EditServiceFragment.class.getClass().getCanonicalName());
    }

    public void enableSave() {
        if (isAdded()) {
            if (mEditAddress.getText().toString().trim().isEmpty() || mEditCity.getText().toString().trim().isEmpty() ||
                    mSpinnerTowState.getSelectedItemPosition() < 0 || mEditZipCode.getText().toString().trim().isEmpty() ||
                    mEditTowDestName.getText().toString().trim().isEmpty() || mSpinnerEditService.getSelectedItemPosition() < 0 ||
                    mSpinnerEditServiceReason.getSelectedItemPosition() < 0) {
                mHomeActivity.saveDisable();
            } else {
                mHomeActivity.saveEnable();
            }
        }
    }
}
