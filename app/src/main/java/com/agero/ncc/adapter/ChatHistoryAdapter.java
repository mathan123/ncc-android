package com.agero.ncc.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.agero.ncc.R;

import com.agero.ncc.model.Profile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import java.util.ArrayList;

public class ChatHistoryAdapter extends RecyclerView.Adapter<ChatHistoryAdapter.ChatHistoryItemHolder> {
    private ArrayList<Profile> mChatUserList;
    private Context mContext;

    public ChatHistoryAdapter(Context mContext, ArrayList<Profile> chatUserList) {
        this.mContext = mContext;
        this.mChatUserList  = chatUserList;

    }

    @Override
    public ChatHistoryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_history_child_item,parent,false);
        return new ChatHistoryItemHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatHistoryItemHolder holder, int position) {

        String firstname = mChatUserList.get(position).getFirstName();
        String lastname = mChatUserList.get(position).getLastName();

        holder.textUserName.setText(firstname + " "+lastname);

        holder.textProfile.setText(String.valueOf(firstname.charAt(0))+String.valueOf(lastname.charAt(0)));
        holder.textProfile.setVisibility(View.VISIBLE);

        if (mChatUserList.get(position).getImageUrl() != null && !mChatUserList.get(position).getImageUrl().isEmpty()) {

            Glide.with(mContext).load(mChatUserList.get(position).getImageUrl()).listener(new RequestListener<Drawable>() {
                @Override public boolean onLoadFailed(@Nullable GlideException e, Object model,
                        Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                        DataSource dataSource, boolean isFirstResource) {
                    holder.textProfile.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.imageProfile);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mChatUserList.size();
    }

    public class ChatHistoryItemHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.user_profile_image)
        ImageView imageProfile;
        @BindView(R.id.image_profile_tv)
        TextView textProfile;
        @BindView(R.id.user_name)
        TextView textUserName;
        public ChatHistoryItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
