package com.agero.ncc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.vehicledetails.MyVehicleDetails;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MakesDetailAdapter extends BaseAdapter {
    MyVehicleDetails makesItems = new MyVehicleDetails();
    Context mContext;
    LayoutInflater inflater;
    int type;
    int selectedMake;
    int selectedModel;
    List<String> years = new ArrayList<>();

    public MakesDetailAdapter(MyVehicleDetails makesItems, Context mContext, int type, int selectedMake, List<String> years) {
        this.makesItems = makesItems;
        this.mContext = mContext;
        this.type = type;
        this.selectedMake = selectedMake;
        this.years = years;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if (type == 1) {
            return makesItems.getMakes().size();
        } else if (type == 2) {
//            if(selectedMake!=0) {
            return makesItems.getMakes().get(selectedMake).getModels().size();
//            }else {
//                return 0;
//            }
        } else {
            return years.size();
        }

    }


    @Override
    public Object getItem(int position) {
        if (type == 1) {
            return makesItems.getMakes().get(position);
        } else if (type == 2) {
            return makesItems.getMakes().get(selectedMake).getModels().get(position);
        } else {
            return years.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_vehicle_details_list, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            holder.mTextMakesName.setTextColor(Color.argb(125, 0, 0, 0));
        } else {
            holder.mTextMakesName.setTextColor(Color.argb(255, 0, 0, 0));
        }
        if (type == 1) {
            holder.mTextMakesName.setText(makesItems.getMakes().get(position).getName());
        } else if (type == 2) {
            holder.mTextMakesName.setText(makesItems.getMakes().get(selectedMake).getModels().get(position).getName());
        } else {
            holder.mTextMakesName.setText(years.get(position));
        }

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.text_makes_name)
        TextView mTextMakesName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
