package com.agero.ncc.app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.model.SparkLocationData;
import com.agero.ncc.retrofit.RetrofitModule;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSdkInterface;
import com.apptentive.android.sdk.Apptentive;
import com.crashlytics.android.Crashlytics;
import com.github.ajalt.reprint.core.Reprint;
import com.google.firebase.database.FirebaseDatabase;
import com.karumi.dexter.Dexter;
import com.splunk.mint.Mint;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zopim.android.sdk.api.ZopimChat;

import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.Configuration;
import co.chatsdk.firebase.FirebaseModule;
import co.chatsdk.firebase.file_storage.FirebaseFileStorageModule;
import co.chatsdk.firebase.push.FirebasePushModule;
import co.chatsdk.ui.manager.UserInterfaceModule;
import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by sdevabhaktuni on 9/19/16.
 */

public class NCCApplication extends MultiDexApplication {

  private static NCCApplication sInstance;

  public static SparkLocationData mLocationData;

  public static NCCApplication getContext() {
    return sInstance;
  }

  private ApplicationComponent mComponent;

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());

    sInstance = this;
    Dexter.initialize(getApplicationContext());
    // init dagger
    mComponent = DaggerApplicationComponent.builder()
            .roomModule(new RoomModule(this))
            .appModule(new AppModule(this))
            .retrofitModule(new RetrofitModule("https://agero-dev.apigee.net/gameon/"))
            .build();
    mComponent.inject(this);

      FirebaseDatabase.getInstance().setPersistenceEnabled(true);

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
    Apptentive.register(this, BuildConfig.APPTENTIVE_APP_KEY, BuildConfig.APPTENTIVE_APP_SIGNATURE);

    NccSdk.getNccSdk(this).authenticate(BuildConfig.SDK_KEY, new NccSdkInterface.AuthenticateCallback() {
      @Override
      public void onSuccess() {
        Log.d("NCCApplication", "Authentication succeeded!");
      }

      @Override
      public void onFailure(Throwable throwable) {
        Log.e("NCCApplication", "Authentication failed!", throwable);
      }
    });
    try {
      NccSdk.getNccSdk(this).setMonitoringNotification(R.mipmap.icon_notifications, "Spark is using your location", "Allows the app to progress job status");
    } catch (NccException e) {
      e.printStackTrace();
    }


    if("DEV".equalsIgnoreCase(BuildConfig.ENV) ||"STAGE".equalsIgnoreCase(BuildConfig.ENV) ) {
      ZendeskConfig.INSTANCE.init(this, "https://agerotest.zendesk.com", "d5e12281bf9749e6c07d38dc665ab6e92e8bd5b5260652f1", "mobile_sdk_client_485bd4357128ac9a82bc");
    } else {
      ZendeskConfig.INSTANCE.init(this, "https://agero6949.zendesk.com", "8614d70d4f1e7d8d282c269ee728aad3fafda48929bc6b09", "mobile_sdk_client_0d5baac09ad95a2a160c");
    }

    Logger.setLoggable(true);


    Identity identity = new AnonymousIdentity.Builder().withNameIdentifier("Generaic").build();
    ZendeskConfig.INSTANCE.setIdentity(identity);

    ZopimChat.init("a3bl88WQ3ROeYXBz6mrGxZX3B1EjCab7");



    Mint.initAndStartSession(sInstance, "a755ce84");
    if("DEV".equalsIgnoreCase(BuildConfig.ENV)) {
      Mint.setApplicationEnvironment(Mint.appEnvironmentDevelopment);
    } else if("STAGE".equalsIgnoreCase(BuildConfig.ENV)) {
      Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
    }else{
      Mint.setApplicationEnvironment(Mint.appEnvironmentRelease);
    }
    Mint.enableDebugLog();

    // Branch logging for debugging
    Branch.enableLogging();

    // Branch object initialization
    Branch.getAutoInstance(this);
    Reprint.initialize(this);
  }

  public ApplicationComponent getComponent() {
    return mComponent;
  }


  public static NCCApplication from(@NonNull Context context) {
    return (NCCApplication) context.getApplicationContext();
  }

  public static void initialiseChatModule(Context context,String rootPath){
    Configuration.Builder builder = new Configuration.Builder(context);
    builder.firebaseRootPath(rootPath);
    builder.googleMaps("AIzaSyCwwtZrlY9Rl8paM0R6iDNBEit_iexQ1aE");
    builder.firebaseCloudMessagingServerKey("AAAAYf_ZMe0:APA91bEVHGE3jGVkgZTzRD2tpmrudgXlLdvb8X7yfqIN_wnJhvhA31OBovJc_zomL3JmpbujDy_O2WJ4ukA_NBIgm3iWA9xii1YVHVx1K3hlhYeQWKBPuXsswVCTUxuTgroGdtnfOUlg");
    builder.publicRoomCreationEnabled(false);
    builder.threadDetailsEnabled(false);
    builder.pushNotificationSound("ringtone");
    builder.locationMessagesEnabled(false);
    builder.pushNotificationImageDefaultResourceId(R.drawable.icon_notifications);
    builder.setClientPushEnabled(false);
    ChatSDK.initialize(builder.build());

    UserInterfaceModule.activate(context);

    FirebaseModule.activate();

    FirebaseFileStorageModule.activate();
    FirebasePushModule.activateForFirebase();
  }
}
