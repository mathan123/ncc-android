package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.MakesDetailAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Vehicle;
import com.agero.ncc.model.vehicledetails.MakesItem;
import com.agero.ncc.model.vehicledetails.ModelsItem;
import com.agero.ncc.model.vehicledetails.MyVehicleDetails;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;


public class AddVehicleDetailsFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    Unbinder unbinder;
    List<String> mMakes, mYears, mModels;
    List<MakesItem> makesItems;
    List<ModelsItem> modelsItems;
    MyVehicleDetails makes;
    String mSelectMakes, mSelectYear, mSelectModels;
    int modelPos;
    @Inject
    NccApi nccApi;
    @BindView(R.id.spinner_vehicle_make)
    Spinner spinnerVehicleMake;
    @BindView(R.id.spinner_vehicle_model)
    Spinner spinnerVehicleModel;
    @BindView(R.id.spinner_vehicle_year)
    Spinner spinnerVehicleYear;
    @BindView(R.id.spinner_vehicle_type)
    Spinner spinnerVehicleType;
    @BindView(R.id.spinner_vehicle_fuel)
    Spinner spinnerVehicleFuel;
    @BindView(R.id.edit_vin_no)
    EditText editVinNumber;
    UserError mUserError;


    public AddVehicleDetailsFragment() {
        // Intentionally empty
    }

    public static AddVehicleDetailsFragment newInstance(boolean isAddNew) {
        AddVehicleDetailsFragment fragment = new AddVehicleDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(NccConstants.BUNDLE_KEY_ADD_NEW, isAddNew);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_vehicle_details, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_vehicle));
        mUserError= new UserError();
        mMakes = new ArrayList<>();
        mYears = new ArrayList<>();
        mModels = new ArrayList<>();
        makesItems = new ArrayList<>();
        modelsItems = new ArrayList<>();

        if(Utils.isNetworkAvailable()){
            int year = Calendar.getInstance().get(Calendar.YEAR);
            mYears.add("Years");
            for (int i = year; i >= 1769; i--) {
                mYears.add("" + i);
            }
            getVehicleDetails();
        }else{
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

        return superView;
    }


    @OnClick(R.id.button_add_vehicle_save)
    public void onViewClicked() {
        Vehicle vehicle = AddJobsDetailsFragment.newJob.getVehicle();
        vehicle.setVin(editVinNumber.getText().toString());
        vehicle.setMake(mSelectMakes);
        vehicle.setModel(mSelectModels);
        vehicle.setYear(Integer.valueOf(mSelectYear));
        vehicle.setVehicleType(spinnerVehicleType.getSelectedItem().toString());
        vehicle.setFuelType(spinnerVehicleFuel.getSelectedItem().toString());

        AddJobsDetailsFragment.newJob.setVehicle(vehicle);
        if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
            mHomeActivity.push(AddCustomerDetailsFragment.newInstance(true),getString(R.string.title_add_customer));
        } else {
            mHomeActivity.onBackPressed();
        }

    }

    private void getVehicleDetails() {
        showProgress();
        nccApi.getVehicleDetails("https://agero-qa.apigee.net/vehicle-data/makes", "I6DT4QIfnh7zNc9ijYU4JS7V3A5db6MH").enqueue(new retrofit2.Callback<MyVehicleDetails>() {
            @Override
            public void onResponse(Call<MyVehicleDetails> call, Response<MyVehicleDetails> response) {
                hideProgress();
                if (response != null && response.body() != null) {
                    makes = response.body();
                    List<ModelsItem> modelsItems = new ArrayList<>();
                    MakesItem makesItem = new MakesItem();
                    ModelsItem modelsItem = new ModelsItem();
                    modelsItem.setName("Select Model");
                    makesItem.setName("Select Make");
                    makesItem.setModels(modelsItems);
                    modelsItems.add(modelsItem);
                    makes.getMakes().add(0, makesItem);
                    MakesDetailAdapter makesDetailAdapter = new MakesDetailAdapter(makes, getActivity(), 1, 0, mYears);
                    spinnerVehicleMake.setAdapter(makesDetailAdapter);

                    MakesDetailAdapter modelDetailAdapter = new MakesDetailAdapter(makes, getActivity(), 3, 0, mYears);
                    spinnerVehicleYear.setAdapter(modelDetailAdapter);
                    spinnerVehicleYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if(parent.getSelectedItem()!=null){
                                mSelectYear = mYears.get(position);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    spinnerVehicleMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                            mSelectMakes = makes.getMakes().get(position).getName();
                            if (!(makes.getMakes().get(position).getModels().get(0).getName().equals("Select Model"))) {
                                ModelsItem models = new ModelsItem();
                                models.setName("Select Model");
                                makes.getMakes().get(position).getModels().add(0, models);
                            }
                            MakesDetailAdapter modelDetailAdapter = new MakesDetailAdapter(makes, getActivity(), 2, spinnerVehicleMake.getSelectedItemPosition(), mYears);
                            spinnerVehicleModel.setAdapter(modelDetailAdapter);
                            spinnerVehicleModel.setSelection(modelPos);
                            modelPos=0;

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }

                    });
                    spinnerVehicleModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mSelectModels = ((ModelsItem)parent.getItemAtPosition(position)).getName();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {

                    } else {
                        editVinNumber.setText(AddJobsDetailsFragment.newJob.getVehicle().getVin());
                        int makePos = getSelectedMakePosition(AddJobsDetailsFragment.newJob.getVehicle());
                        modelPos = getSelectedModelPosition(AddJobsDetailsFragment.newJob.getVehicle(), makePos);
                        spinnerVehicleMake.setSelection(makePos);
                        spinnerVehicleYear.setSelection(getSelectedYearPosition(AddJobsDetailsFragment.newJob.getVehicle()));
                    }
                }
            }

            @Override
            public void onFailure(Call<MyVehicleDetails> call, Throwable t) {
                if(isAdded()) {
                    mHomeActivity.mintlogEvent("Add Vehicle Details Api Error - " + t.getLocalizedMessage());
                }
                hideProgress();
            }


        });
    }

    int getSelectedMakePosition(Vehicle vehicle) {
        for (int i = 0; i < makes.getMakes().size(); i++) {
            if (vehicle.getMake().equalsIgnoreCase(makes.getMakes().get(i).getName())) {
                return i;
            }
        }
        return 0;
    }



    int getSelectedModelPosition(Vehicle vehicle, int makePos) {
        for (int i = 0; i < makes.getMakes().get(makePos).getModels().size(); i++) {
            if (vehicle.getModel().equalsIgnoreCase(makes.getMakes().get(makePos).getModels().get(i).getName())) {
                return i + 1;
            }
        }
        return 0;
    }

    int getSelectedYearPosition(Vehicle vehicle) {
        for (int i = 0; i < mYears.size(); i++) {
            if (vehicle.getYear() == Integer.valueOf(mYears.get(i))) {
                return i;
            }
        }
        return 0;
    }

}
