package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * To show hint for getting signature with full screen, semi-transparent activity.
 */
public class SignatureNavigationActivity extends BaseActivity {
    @BindView(R.id.constraint_signature_navigation)
    ConstraintLayout mConstraintSignatureNavigation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_navigation);
        ButterKnife.bind(this);
        mConstraintSignatureNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
